'use strict'
//Import Express
const express = require('express');
//user router
const router = express.Router();
//Import body parser
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

//import  controllers
const userController = require('../src/user/usersController');
const locationDataController = require('../src/driver-model/locationData/locationDataController');
const journeyController = require('../src/driver-model/journeyData/journeyController');
const trackingController = require('../src/driver-model/busTracking/trackingController');
const busRouteController = require('../src/driver-model/busRoute/busRouteController');
const busController = require('../src/bus/busController');
const scheduleController = require('../src/bus-allocation/scheduleController');
const predictionController = require('../src/driver-model/passengerPrediction/passengerPredictionController');


//import Validator class
const validator = require('../validators/validator');

//import validator Schemas 
const userSchema = require('../src/user/userSchema');
const busRouteSchema = require('../src/driver-model/busRoute/busRouteSchema');
const busSchema = require('../src/bus/busSchema');
const trackerSchema = require('../src/driver-model/busTracking/trackingSchema');
const scheduleSchema = require('../src/bus-allocation/scheduleSchema');
const predictionSchema = require('../src/driver-model/passengerPrediction/passengerPredictionSchema');

//user routes
router.route('/api/user/new').post(validator.validateBody(userSchema.newUser), userController.newUser);
router.route('/api/user/login').post(validator.validateBody(userSchema.login), userController.login);
router.route('/api/user/changePassword').put(validator.validateBodyWithToken(userSchema.changePassword), userController.changePassword);
router.route('/api/user/remove').post(validator.validateBodyWithToken(userSchema.user_id), userController.RemoveUser);
router.route('/api/user/users').get(validator.validateHeader(), userController.getUsers);
router.route('/api/user/getUser').post(validator.validateBodyWithToken(userSchema.user_id), userController.getUserById);
router.route('/api/user/update').post(validator.validateBodyWithToken(userSchema.updateUser), userController.updateUser);

//locationData routes
router.route('/api/cordinate/new').post(locationDataController.newCordinate);
router.route('/api/cordinate/getRawData').post(locationDataController.getRawCordinates);

//tracking routes
router.route('/api/tracking/new').post(validator.validateBodyWithToken(trackerSchema.newTracker), trackingController.newTracking);
router.route('/api/tracking/update').put(validator.validateBodyWithToken(trackerSchema.updateTracker), trackingController.updateTracker);
router.route('/api/tracking/get').get(validator.validateHeader(), trackingController.getTrackers);
router.route('/api/tracking/route').post(validator.validateBodyWithToken(trackerSchema.route), trackingController.getRouteActiveTrackers);
router.route('/api/tracking/remove').post(validator.validateBodyWithToken(trackerSchema.trackerID), trackingController.removeTracker);

//journey routes
router.route('/api/journey/create').post(journeyController.startJourney);
router.route('/api/journey/end').post(journeyController.journeyEnd);
router.route('/api/journey/get').post(journeyController.journeyEnd);

//busRoute route
router.route('/api/route/new').post(validator.validateBodyWithToken(busRouteSchema.newRoute), busRouteController.newBusRoute);
router.route('/api/route/edit').post(validator.validateBodyWithToken(busRouteSchema.editRoute), busRouteController.updateRoute);
router.route('/api/route/findByID').post(validator.validateBodyWithToken(busRouteSchema.route_id), busRouteController.findByID);
router.route('/api/route/findByRouteNo').post(validator.validateBodyWithToken(busRouteSchema.route_no), busRouteController.findByRouteNo);
router.route('/api/route/remove').post(validator.validateBodyWithToken(busRouteSchema.route_id), busRouteController.removeRoute);
router.route('/api/route/getAll').get(validator.validateHeader(), busRouteController.getAllRoutes);

//bus route
router.route('/api/bus/new').post(validator.validateBodyWithToken(busSchema.newBus), busController.newBus);
router.route('/api/bus/update').put(validator.validateBodyWithToken(busSchema.updateBus), busController.updateBus);
router.route('/api/bus/get').get(busController.getBuses);
router.route('/api/bus/findByBusNo').post(validator.validateBodyWithToken(busSchema.busNo), busController.findFromBusNo);
router.route('/api/bus/findByBusID').post(validator.validateBodyWithToken(busSchema.busID), busController.findFromID);
router.route('/api/bus/remove').delete(validator.validateBodyWithToken(busSchema.busID), busController.removeBus);
router.route('/api/bus/activeBusesFromRoute').post(validator.validateBodyWithToken(busSchema.activeBus), busController.getActiveBusesFromRoute);


//schedules
router.route('/api/schedule/generateSchedule').post(validator.validateBodyWithToken(scheduleSchema.createSchedule), scheduleController.createSchedule);
router.route('/api/passenger/prediction').get(predictionController.getPrediction);
router.route('/api/passenger/getPredictedData').post(validator.validateBodyWithToken(predictionSchema.getPredictedData), predictionController.getPredictedData);
router.route('/api/schedule/get').post(scheduleController.getSchedule);

module.exports = router;
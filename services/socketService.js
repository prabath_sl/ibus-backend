//import required libraries
const socketio = require('socket.io');
const trackingService = require('../src/driver-model/busTracking/trackingService');
const journeyService = require('../src/driver-model/journeyData/journeyService');
const locationService = require('../src/driver-model/locationData/locationDataService');
const busService = require('../src/bus/busService');
//define global variable
let io;
let connectedUsers = [];

//initialize the socket
function listen(app) {
    io = socketio.listen(app);
    io.on('connection', socket => {
        console.log('user connected');
        socket.on('create', function (user) {
            console.log(user);
        });

        socket.on('disconnect', function () {
            console.log('user disconnected');
            var count = 0
            connectedUsers.forEach(element => {
                if (element.socket_id == socket.id) {
                    connectedUsers.splice(count, 1);
                }
                count++;
            });
            console.log(connectedUsers)
        });

        socket.on('location', (locationObj) => {
            console.log(locationObj)
            var obj = {};
            obj.status = "Active";
            obj.bus_id = locationObj.bus;
            trackingService.checkActiveTracker(obj, "res", (data) => {
                console.log(data)
                if (data.status) {
                    if (locationObj.status == "Deactive") {
                        journeyService.journeyEnd({
                            bus_id: locationObj.bus,
                            tracker: data.data._id,
                            endTime: locationObj.endTime,
                            total_KM: locationObj.total_km
                        }, "", (journey) => {
                            var location = {};
                            location.longitude = data.data.longitude;
                            location.latitude = data.data.latitude;
                            location.status = "Deactive";
                            location.tracker_id = data.data._id;
                            busStatus({ bus_id: locationObj.bus, status: "Deactivate" })
                            tracker(location);
                            location.tracker_id = data.data._id;
                            location.bus = locationObj.bus;
                            mineLocationData(location);
                        })
                    } else {
                        var location = {};
                        location.longitude = locationObj.longitude;
                        location.latitude = locationObj.latitude;
                        location.status = "Active";
                        location.tracker_id = data.data._id;
                        tracker(location);
                        location.bus = locationObj.bus;
                        mineLocationData(location);
                    }
                } else {
                    var location = {};
                    location.longitude = locationObj.longitude;
                    location.latitude = locationObj.latitude;
                    location.status = "Active";
                    location.bus = locationObj.bus;
                    location.route_id = locationObj.route_id;
                    location.route_no = "157";
                    location.start_location = locationObj.start_location;
                    trackingService.startTracking(location, " ", (tracker) => {
                        if (tracker.status) {
                            var journey = {};
                            journey.startTime = locationObj.startTime
                            journey.bus = locationObj.bus;
                            journey.tracker = tracker.data._id;
                            journeyService.startJourney(journey, "", (journeyStatus) => {
                                if (journeyStatus.status) {
                                    location.tracker_id = tracker.data._id
                                    mineLocationData(location);
                                    busStatus({ bus_id: locationObj.bus, status: "Active" });
                                    emitActiveTrackers();
                                } else {
                                    emitActiveTrackers()
                                    console.log(journeyStatus);
                                }
                            });
                        } else {
                            console.log(tracker);
                        }
                    });
                }
            });
        });

        socket.on('getUser', function (user) {
            console.log(user);
            connectedUsers.push({ user_id: user.id, socket_id: socket.id })
            console.log(connectedUsers)
        })
    });
    return io;
}

function emitActiveTrackers() {
    trackingService.getActiveTrackers("", "", (trackers) => {
        if (trackers.status) {
            io.emit('trackers', { type: 'trackers', tracker: trackers.data });
        } else {
            io.emit('trackers', { type: 'trackers', tracker: trackers.data });
        }
    });
}

function mineLocationData(location) {

    locationService.newCordinate(location, " ", (trackers) => {
        if (trackers.status) {
            emitActiveTrackers()
        } else {
            emitActiveTrackers()
        }
    });
}

function tracker(location) {
    trackingService.trackingUpdate(location, "res", (updatedTracker) => {
        if (updatedTracker.status) {
            emitActiveTrackers()
            console.log(updatedTracker.data);
        } else {
            emitActiveTrackers()
            console.log(updatedTracker.data);
        }
    });
}

function busStatus(data) {
    busService.updateBus({ body: { onGoingStatus: data.status, bus_id: data.bus_id } }, "res", function (data) {
        if (data.status) {
            console.log(data.data);
        } else {
            console.log(data.data);
        }
    })
}

//export functions
module.exports = {
    listen,
    io,
    connectedUsers
}
var config = {};

config.web_port = 5000;
config.database = "//localhost:27017/IBus";
config.passengerPredictionAPI = "http://localhost:5000/passengerCount/predict"

module.exports = config;
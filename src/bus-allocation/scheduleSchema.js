//import validator class
const joi = require('joi');

//new bus Schema and validations to be done 
module.exports.createSchedule = joi.object().keys({
    recreate: joi.boolean().required(),
    iterationCount: joi.number().required(),
    route_no: joi.string().required(),
    date:joi.required(),
});
